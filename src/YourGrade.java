import java.util.Scanner;

public class YourGrade {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please enter your grade : ");
        int yourGrade = sc.nextInt();
        
        if (yourGrade >= 80){
            System.out.println("A");
        }else if (yourGrade >= 75 && yourGrade < 80){
            System.out.println("B+");
        }else if (yourGrade >= 70 && yourGrade < 75){
            System.out.println("B");
        }else if (yourGrade >= 65 && yourGrade < 70){
            System.out.println("C+");
        }else if (yourGrade >= 60 && yourGrade < 65){
            System.out.println("C");
        }else if (yourGrade >= 55 && yourGrade < 60){
            System.out.println("D+");
        }else if (yourGrade >= 50 && yourGrade < 55){
            System.out.println("D");
        }else{
            System.out.println("F");
        }
    }
}
